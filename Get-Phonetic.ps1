
#
# Support
# v. 0.1 20170809 P.Fijma
#

    <#
      .Synopsis
       Generates a table with phonetic spelling from a collection of characters
      .DESCRIPTION
       Generates a table with phonetic spelling from a collection of characters
      .EXAMPLE
       "gjIgsj" | Get-Phonetic

       Input text: gjIgsj

       Char Phonetic
       ---- --------
          g golf    
          j juliett 
          I INDIA   
          g golf    
          s sierra  
          j juliett 
       
      .OUTPUTS
       [String]
      .NOTES
       Written by Simon Wåhlin, blog.simonw.se
       I take no responsibility for any issues caused by this script.
    #>
    Param (
        # List of characters to translate to phonetic alphabet
        [Parameter(Mandatory=$true,ValueFromPipeLine=$true)]
        [Char[]]$Char,
        # Hashtable containing a char as key and phonetic word as value
        [HashTable]$PhoneticTable = @{
            'a' = 'alpha'   ;'b' = 'bravo'   ;'c' = 'charlie';'d' = 'delta';
            'e' = 'echo'    ;'f' = 'foxtrot' ;'g' = 'golf'   ;'h' = 'hotel';
            'i' = 'india'   ;'j' = 'juliett' ;'k' = 'kilo'   ;'l' = 'lima' ;
            'm' = 'mike'    ;'n' = 'november';'o' = 'oscar'  ;'p' = 'papa' ;
            'q' = 'quebec'  ;'r' = 'romeo'   ;'s' = 'sierra' ;'t' = 'tango';
            'u' = 'uniform' ;'v' = 'victor'  ;'w' = 'whiskey';'x' = 'x-ray';
            'y' = 'yankee'  ;'z' = 'zulu'    ;'0' = 'Zero'   ;'1' = 'One'  ;
            '2' = 'Two'     ;'3' = 'Three'   ;'4' = 'Four'   ;'5' = 'Five' ;
            '6' = 'Six'     ;'7' = 'Seven'   ;'8' = 'Eight'  ;'9' = 'Niner';
            '.' = 'Point'   ;'!' = 'Exclamationmark';'?' = 'Questionmark';
        }
    )

    Process {
        $Result = Foreach($Character in $Char) {
            if($PhoneticTable.ContainsKey("$Character")) {
                if([Char]::IsUpper([Char]$Character)) {
                    [PSCustomObject]@{
                        Char = $Character;Phonetic = $PhoneticTable["$Character"].ToUpper()
                    }
                }
                else {
                    [PSCustomObject]@{
                        Char = $Character;Phonetic = $PhoneticTable["$Character"].ToLower()
                    }
                }
            }
            else {
                [PSCustomObject]@{
                    Char = $Character;Phonetic = $Character
                }
            }
            
        }
        "`n{0}`n{1}" -f ('Input text: {0}'-f-join$Char), ($Result | Format-Table -AutoSize | Out-String)
    }

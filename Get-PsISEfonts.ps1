
# source: https://blogs.technet.microsoft.com/heyscriptingguy/2010/08/22/changing-the-fonts-used-by-the-windows-powershell-ise/

Function Get-Fonts 
{ 
Param($fontSize = 16,  
$milliSecs = 750 
) 
[windows.media.fonts]::systemTypeFaces |  
Select-Object -Property fontFamily -Unique | 
ForEach-Object { 
$_.fontFamily.source 
$psISE.Options.FontName = $_.fontFamily.source 
$psISE.Options.FontSize = $fontSize 
Start-Sleep -Milliseconds $millisecs } 
} #function get-fonts 
# *** entrypoint to script *** 
$font = $psISE.Options.FontName 
$size = $psISE.Options.FontSize 
get-fonts -fontSize 14 -milliSecs 500 
$psise.options.FontName = $font 
$psISE.Options.FontSize = $size

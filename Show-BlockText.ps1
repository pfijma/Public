<# 
  .DESCRIPTION
  simple function to show a message 
  .NOTES
  
  alt codes to form line square
  # 218,196,191
  # 179,032,179
  # 192,196,217
  
  fillers: 
  #   (alt-code)
  # ░ 176 
  # ▒ 177 
  # ▓ 178
  
  .EXAMPLES
  Show-BlockText 'Paul Fijma'
  Show-BlockText 'Po'     #too short, minimum is 3
  #set linewidth to specified size
  Show-BlockText $message -linewidth 120  
#>

function Show-BlockText{
  param(
    [Parameter(Position=1)]
    [ValidateLength(3,234)]
    [string]$message=(get-date -format 'yyyy-MM-dd HH:mm.ss'),
    [ValidateRange(40,240)]  
    [int]$linewidth = 80,
    [ValidateLength(1,1)]
    [string]$filler = '▒' #alt-177
  )

if ($message.Length -gt $linewidth-4) {
    #message is langer dan de max ingestelde linewidth
    
    $message.Length
    $msg = $message.subString(0,$linewidth-16)  
    $msg+='...'
    #$msg 
    #$msg.length
    
  }else{$msg = $message}
    
  [int]$stub = [math]::floor(($linewidth-$msg.Length)/2)
  
  if($stub+$msg.Length+$stub -eq $linewidth){
    $f = $filler
  }else{
    $msg+=' '
    $f=$filler
  }
  
  if ($stub -eq $linewidth){$stub+=1}
  
  $msg = $f*$stub+' '+$msg+' '+$f*$stub
  $lCnt = $msg.length
  $lTop = '┌'+'─'*$lcnt+'┐'
  $lMid = '│'+('{0}'-f $msg) +'│' 
  $lBot = '└'+'─'*$lcnt+'┘'

  $output=$ltop+"`n"+$lmid+"`n"+$lbot  
  return $output
  
}

##############################

#[string]$message = 'ik ben een dinges die erg lang is en deze moet dus zo lang zijn dat hij afgekapt word ergens in de regel'
#$message.Length


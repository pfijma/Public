/*-----------------------------------------------------*/
-- running backup/restore jobs
SELECT session_id as SPID, 
  command, 
  a.text AS Query, 
  start_time, 
  percent_complete, 
  dateadd(second,estimated_completion_time/1000, 
  getdate()) as estimated_completion_time 
FROM sys.dm_exec_requests r CROSS APPLY sys.dm_exec_sql_text(r.sql_handle) a 
WHERE r.command in ('BACKUP DATABASE','RESTORE DATABASE')


/*-----------------------------------------------------*/
--record stae time backup status
select b.database_name, b.backup_start_date, b.backup_finish_date
  from msdb.dbo.backupset b
  order by backup_start_date desc


/*-----------------------------------------------------*/
-- Find any failed job for today's date
SELECT  'Job: ' + Job.[name], 
	Hst.[sql_message_id], 
	Hst.[message] , 
	Hst.[run_date], 
	Hst.[run_time],
	'Hist', 
	hst.*
FROM [msdb].dbo.sysjobhistory Hst  
INNER JOIN [msdb].dbo.sysjobs Job ON Hst.[job_id] = Job.[job_id] 
where hst.run_status = '0'   -- 0 = FAILED
and  convert(varchar(8), GETDATE(),112) = Hst.[run_date] --today
--and  convert(varchar(8), GETDATE()-1,112) = Hst.[run_date] --yesterday (example)
ORDER BY Job.[name],Hst.[run_date] DESC, Hst.[run_time] DESC


/*-----------------------------------------------------*/

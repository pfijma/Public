# Several PowerShell commandlets take a PSCredential object to run using a particular user account. You can create the PSCredential object by using Get-Credential commandlet which opens a dialog to enter the username and password. This way of entering credentials can be used in an interactive mode.

$mycredentials = Get-Credential

#When you have to provide credentials in non-interactive mode, you can create a PSCredential object in the following way.

$secpasswd = ConvertTo-SecureString "PlainTextPassword" -AsPlainText -Force
$mycreds = New-Object System.Management.Automation.PSCredential ("username", $secpasswd)

<#
You can now pass $mycreds to any -PSCredential input parameter
* You may not want to reveal PlainTextPassword for your personal accounts as a general precaution. Use a common user account and password that everyone knows for your non-interactive PSCredential usage.
* Tested with PowerShell 2.0
#>

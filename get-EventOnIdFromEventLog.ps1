
<#
get a specified eventid from eventlog
#>

$filename = 'log_{0}_{1}.txt' -f $env:COMPUTERNAME,(get-date).ToShortDateString()

Get-EventLog -LogName application -EntryType error -After (get-date).AddDays(-180).ToShortDateString() | Where-Object {$_.EventID -eq 18210} | fl | out-file .\$filename
invoke-item .\$filename

#source: https://www.itprotoday.com/windows-8/powershell-event-id-searching 


import-module ActiveDirectory -ErrorAction stop 

#get ALL servers from AD (see examples above to filter specifics)
$Servers = Get-ADComputer -Filter {OperatingSystem -Like 'Windows Server*'} -Property name
$servers.count
#$servers.name | sort

#output files
$fn1 = 'AD_servers_offline_1.txt'
$fn2 = 'AD_servers_online_1.txt'

# create fresh copies of the output files
$null | out-file .\$fn1 -force
$null | out-file .\$fn2 -force

#find longest name to make neat list
$sl = ($servers.name | Measure-Object -Maximum -Property Length).Maximum + 1

#fill the outputfiles with (sorted output)
foreach ($server in ($servers.name | sort)){

    #fastest way to check online or offline
    if(!(Test-Connection -Cn  $server -BufferSize 16 -Count 1 -ea 0 -quiet)){
       
        "{0,-$sl} offline " -f $server 
        $server | out-file .\$fn1 -append

    } else { 
        
        "{0,-$sl} online" -f $server 
        $server | out-file .\$fn2 -append

    }

}

#see results
invoke-item .\$fn1
invoke-item .\$fn2

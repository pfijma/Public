
 # Reboot Pending?
 (Invoke-WmiMethod -Namespace root\ccm\clientsdk -Class CCM_ClientUtilities -Name DetermineIfRebootPending).RebootPending

 # boottimes via eventlog 
 Get-WmiObject Win32_NTLogEvent -filter "LogFile='System' and EventCode=6005" | Select ComputerName, EventCode, @{LABEL='TimeWritten';EXPRESSION= {$_.ConverttoDateTime($_.TimeWritten)}}

 #last boottime
 Get-CimInstance -ClassName win32_operatingsystem | select csname, lastbootuptime

Get-ADUser Test.User -Properties * | Select-Object -ExpandProperty description

Get-ADUser -Identity GarySmith -Properties DisplayName,Description | ForEach-Object {
    $_.Displayname
    $_.Description
} 


# 
# create the manifest file:
# New-ModuleManifest f:\px\Q\PxDeployment.psd1 -ModuleVersion "2.0" -Author "Paul Fijma"
#

#set-location d:\deploy\1.1a\
#set-location $PSScriptRoot

#$modules = 'auxillary','cloudflare','pxdeploy','pxcmdb','kemp'

$modules = 'TestIT' #'kemp'

#(mod auto fills the path, leave empty to run in same folder)

$pathToModule = '' #fill in 'TimeIT' to TestIT.psd1 
#set-location .\inc\

foreach ($module in $modules){
  write-host $module -ForegroundColor white -BackgroundColor red   
  write-verbose "load: .\$pathToModule\$module"
    import-module -name .\$pathToModule\$module -force -verbose    
  (get-module $module).ExportedCommands.Keys
}
get-date

#use force to fresh load the module every time

#slog 'running mainprogram'
#tlog 'TEST'

#tlog 'load modules:'
#(get-module).Name
#get-module -listavailable

# last updated 20181115 17:05
